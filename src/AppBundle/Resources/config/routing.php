<?php
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;

$collection = new RouteCollection();
$collection->add('list_all_airports', new Route('airports', array(
    '_controller' => 'AppBundle:Airports:listAllAirports',
)));

$collection->add('list_all_search', new Route('search', array(
    '_controller' => 'AppBundle:Search:listAllSearch',
)));

return $collection;