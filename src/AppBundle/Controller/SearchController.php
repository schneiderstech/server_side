<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class SearchController extends Controller
{
    /**
     * @Route("/search")
     */
    public function listAllSearchAction()
    {
        try {

            $searchAll = $this->getDoctrine()
                ->getRepository('AppBundle:Search')
                ->findAll();

            $response = array();

            foreach ($searchAll as $search) {
                $response[] = array(
                    'Flight #' => $search->getFlightNum(),
                    // other fields
                );
            }            

            return new JsonResponse($response);

        } catch (\Exception $exception) {

            return new JsonResponse([
                'success' => false,
                'code'    => $exception->getCode(),
                'message' => $exception->getMessage(),
            ]);
        }
    }
    /**
     * @Route("/search/airlines")
     */
    public function listAllAirlinesAction()
    {
        try {

            $allAirlines = $this->getDoctrine()
                ->getRepository('AppBundle:Airlines')
                ->findAll();

            $response = array();

            foreach ($allAirlines as $search) {
                $response[] = array(
                    $search->getCode(),
                    // other fields
                );
            }            

            return new JsonResponse($response);

        } catch (\Exception $exception) {

            return new JsonResponse([
                'success' => false,
                'code'    => $exception->getCode(),
                'message' => $exception->getMessage(),
            ]);
        }
    }

    /**
     * @Route("/flight_search/airline_code={code}")
     */
    public function listAllFlightPerAirlineAction($code)
    {
        try {

            $em = $this->getDoctrine()->getManager();
            $airlines = $em->getRepository('AppBundle:Search')
                ->findAirlineByCode($code);

            $response = array();
            
            foreach ($airlines as $airline) {
                $response[] = array(
                    'airline' => $airline->getAirline(),
                    // other fields
                );
            }            

            return new JsonResponse($response);

        } catch (\Exception $exception) {

            return new JsonResponse([
                'success' => false,
                'code'    => $exception->getCode(),
                'message' => $exception->getMessage(),
            ]);
        }
    }   

    /**
     * @Route("flight_search/date={dateSelected}&from={localFrom}&to={localTo}&range={dateRange}")
     */
    public function listFlightPerPeriodAction($dateSelected, $localFrom, $localTo, $dateRange)
    {
        try {

            $em = $this->getDoctrine()->getManager();
            $searchFlight = $em->getRepository('AppBundle:Search')
                ->findFlightPerPeriod($dateSelected, $localFrom, $localTo, $dateRange);

            return new JsonResponse($searchFlight);    

        } catch (\Exception $exception) {

            return new JsonResponse([
                'success' => false,
                'code'    => $exception->getCode(),
                'message' => $exception->getMessage(),
            ]);
        }
    }    
}
