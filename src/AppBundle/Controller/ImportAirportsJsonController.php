<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Airports;


class ImportAirportsJsonController extends Controller
{
    /**
     * @Route("import-airports")
     */
    public function importAirportsJsonAction()
    {
        $airportsJson = array(
            array(  
                "airportCode" => "MLB",
                "airportName" => "Melbourne International Arpt",
                "cityCode" => "MLB",
                "cityName" => "Melbourne",
                "countryCode" => "US",
                "countryName" => "United States",
                "latitude" => 28.102753,
                "longitude" => -80.645258,
                "stateCode" => "FL",
                "timeZone" => "America/New_York"),
            array(
                "airportCode" => "MEL",
                "airportName" => "Tullamarine Arpt",
                "cityCode" => "MEL",
                "cityName" => "Melbourne",
                "countryCode" => "AU",
                "countryName" => "Australia",
                "latitude" => -37.673333,
                "longitude" => 144.843333,
                "stateCode" => "VI",
                "timeZone" => "Australia/Hobart"                
            )
        );

        for($i = 0, $size = count($airportsJson); $i < $size; ++$i) {
            $airports = new Airports();
            $airports->setAirportCode($airportsJson[$i]['airportCode']);
            $airports->setAirportName($airportsJson[$i]['airportName']);
            $airports->setCityCode($airportsJson[$i]['cityCode']);
            $airports->setCityName($airportsJson[$i]['cityName']);
            $airports->setCountryCode($airportsJson[$i]['countryCode']);
            $airports->setCountryName($airportsJson[$i]['countryName']);
            $airports->setLatitude($airportsJson[$i]['latitude']);
            $airports->setLongitude($airportsJson[$i]['longitude']);
            $airports->setStateCode($airportsJson[$i]['stateCode']);
            $airports->setTimeZone($airportsJson[$i]['timeZone']);

            $em = $this->getDoctrine()->getManager();

            // tells Doctrine you want to (eventually) save the Airports (no queries yet)
            $em->persist($airports);

            // actually executes the queries (i.e. the INSERT query)
            $em->flush();

        }

        return new Response('Saved Airports Json');

    }

}
