<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Airlines;


class ImportAirlinesJsonController extends Controller
{
    /**
     * @Route("/import-airlines")
     */
    public function importAirlinesJsonAction()
    {

        $airlinesJson = array(
            array("code" => "SU", "name" => "Aeroflot"),
            array("code" => "MU", "name" => "China Eastern"),
            array("code" => "EK", "name" => "Emirates"),
            array("code" => "KE", "name" => "Korean Air lines"),
            array("code" => "QF", "name" => "Qantas"),
            array("code" => "SQ", "name" => "Singapore Airlines"));

        for($i = 0, $size = count($airlinesJson); $i < $size; ++$i) {
            $airlines = new Airlines();
            $airlines->setCode($airlinesJson[$i]['code']);
            $airlines->setName($airlinesJson[$i]['name']);

            $em = $this->getDoctrine()->getManager();

            // tells Doctrine you want to (eventually) save the Airlines (no queries yet)
            $em->persist($airlines);

            // actually executes the queries (i.e. the INSERT query)
            $em->flush();

        }

        return new Response('Saved Airlines Json');
        
    }

}
