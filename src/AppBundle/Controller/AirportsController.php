<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class AirportsController extends Controller
{
    /**
     * @Route("/airports")
     */
    public function listAllAirportsAction()
    {

        try {

            $repository = $this->getDoctrine()->getRepository('AppBundle:Airports');

            $airports = $repository->findAll();

            $response = array();
            foreach ($airports as $airport) {
                $response[] = array(
                    'airportCode' => $airport->getAirportCode(),
                    'airportName' => $airport->getAirportName()
                    // other fields
                );
            }            
            return new JsonResponse($response);

        } catch (\Exception $exception) {

            return new JsonResponse([
                'success' => false,
                'code'    => $exception->getCode(),
                'message' => $exception->getMessage(),
            ]);

        }                
    }

    /**
     * @Route("/airport={id}")
     */
    public function getAirportByIdAction($id)
    {

        try {

            $airports = $this->getDoctrine()
                ->getRepository('AppBundle:Airports')
                ->find($id);

            return new JsonResponse($airports->getAirportCode());

        } catch (\Exception $exception) {

            return new JsonResponse([
                'success' => false,
                'code'    => $exception->getCode(),
                'message' => $exception->getMessage(),
            ]);
        }
    }

    /**
     * @Route("/airport/airportName={name}")
     */
    public function getAirportByNameAction($name)
    {

        try {

            $em = $this->getDoctrine()->getManager();
            $airports = $em->getRepository('AppBundle:Airports')
                ->findAirportByName($name);

            $response = array();
            foreach ($airports as $airport) {
                $response[] = array(
                    'airportCode' => $airport->getAirportCode()
                    // other fields
                );
            }            

            return new JsonResponse($response);

        } catch (\Exception $exception) {

            return new JsonResponse([
                'success' => false,
                'code'    => $exception->getCode(),
                'message' => $exception->getMessage(),
            ]);
        }
    }

}
