<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class AirlinesController extends Controller
{
    /**
     * @Route("/airlines")
     */
    public function listAllAirlinesAction()
    {
        try {

            $repository = $this->getDoctrine()->getRepository('AppBundle:Airlines');

            $airlines = $repository->findAll();

            $response = array();
            foreach ($airlines as $airline) {
                $response[] = array(
                    'code' => $airline->getCode(),
                    'name' => $airline->getName()
                );
            }            
            return new JsonResponse($response);

        } catch (\Exception $exception) {

            return new JsonResponse([
                'success' => false,
                'code'    => $exception->getCode(),
                'message' => $exception->getMessage(),
            ]);

        }    
    }

    /**
     * @Route("/airline={id}")
     */
    public function getAirportByIdAction($id)
    {

        try {

            $airline = $this->getDoctrine()
                ->getRepository('AppBundle:Airlines')
                ->find($id);

            return new JsonResponse($airline->getCode());

        } catch (\Exception $exception) {

            return new JsonResponse([
                'success' => false,
                'code'    => $exception->getCode(),
                'message' => $exception->getMessage(),
            ]);
        }
    }
}
