<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Airports
 *
 * @ORM\Table(name="airports")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AirportsRepository")
 */
class Airports
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="airportCode", type="string", length=255)
     */
    private $airportCode;

    /**
     * @var string
     *
     * @ORM\Column(name="airportName", type="string", length=255)
     */
    private $airportName;

    /**
     * @var string
     *
     * @ORM\Column(name="cityCode", type="string", length=255)
     */
    private $cityCode;

    /**
     * @var string
     *
     * @ORM\Column(name="cityName", type="string", length=255)
     */
    private $cityName;

    /**
     * @var string
     *
     * @ORM\Column(name="countryCode", type="string", length=255)
     */
    private $countryCode;

    /**
     * @var string
     *
     * @ORM\Column(name="countryName", type="string", length=255)
     */
    private $countryName;

    /**
     * @var decimal
     *
     * @ORM\Column(name="latitude", type="decimal", precision=18, scale=6)
     */
    private $latitude;

    /**
     * @var decimal
     *
     * @ORM\Column(name="longitude", type="decimal", precision=18, scale=6)
     */
    private $longitude;

    /**
     * @var string
     *
     * @ORM\Column(name="stateCode", type="string", length=255)
     */
    private $stateCode;

    /**
     * @var string
     *
     * @ORM\Column(name="timeZone", type="string", length=255)
     */
    private $timeZone;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set airportCode
     *
     * @param string $airportCode
     *
     * @return Airports
     */
    public function setAirportCode($airportCode)
    {
        $this->airportCode = $airportCode;

        return $this;
    }

    /**
     * Get airportCode
     *
     * @return string
     */
    public function getAirportCode()
    {
        return $this->airportCode;
    }

    /**
     * Set airportName
     *
     * @param string $airportName
     *
     * @return Airports
     */
    public function setAirportName($airportName)
    {
        $this->airportName = $airportName;

        return $this;
    }

    /**
     * Get airportName
     *
     * @return string
     */
    public function getAirportName()
    {
        return $this->airportName;
    }

    /**
     * Set cityCode
     *
     * @param string $cityCode
     *
     * @return Airports
     */
    public function setCityCode($cityCode)
    {
        $this->cityCode = $cityCode;

        return $this;
    }

    /**
     * Get cityCode
     *
     * @return string
     */
    public function getCityCode()
    {
        return $this->cityCode;
    }

    /**
     * Set cityName
     *
     * @param string $cityName
     *
     * @return Airports
     */
    public function setCityName($cityName)
    {
        $this->cityName = $cityName;

        return $this;
    }

    /**
     * Get cityName
     *
     * @return string
     */
    public function getCityName()
    {
        return $this->cityName;
    }

    /**
     * Set countryCode
     *
     * @param string $countryCode
     *
     * @return Airports
     */
    public function setCountryCode($countryCode)
    {
        $this->countryCode = $countryCode;

        return $this;
    }

    /**
     * Get countryCode
     *
     * @return string
     */
    public function getCountryCode()
    {
        return $this->countryCode;
    }

    /**
     * Set countryName
     *
     * @param string $countryName
     *
     * @return Airports
     */
    public function setCountryName($countryName)
    {
        $this->countryName = $countryName;

        return $this;
    }

    /**
     * Get countryName
     *
     * @return string
     */
    public function getCountryName()
    {
        return $this->countryName;
    }

    /**
     * Set latitude
     *
     * @param decimal(18,12) $latitude
     *
     * @return Airports
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return decimal(18,12)
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set longitude
     *
     * @param decimal(18,12) $longitude
     *
     * @return Airports
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return decimal(18,12)
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set stateCode
     *
     * @param string $stateCode
     *
     * @return Airports
     */
    public function setStateCode($stateCode)
    {
        $this->stateCode = $stateCode;

        return $this;
    }

    /**
     * Get stateCode
     *
     * @return string
     */
    public function getStateCode()
    {
        return $this->stateCode;
    }

    /**
     * Set timeZone
     *
     * @param string $timeZone
     *
     * @return Airports
     */
    public function setTimeZone($timeZone)
    {
        $this->timeZone = $timeZone;

        return $this;
    }

    /**
     * Get timeZone
     *
     * @return string
     */
    public function getTimeZone()
    {
        return $this->timeZone;
    }
}

