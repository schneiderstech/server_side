<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Search
 *
 * @ORM\Table(name="search")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SearchRepository")
 */
class Search
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="searchKey", type="string", length=255)
     */
    private $searchKey;

    /**
     * @var string
     *
     * @ORM\Column(name="airline", type="string", length=10)
     */
    private $airline;

    /**
     * @var int
     *
     * @ORM\Column(name="flightNum", type="integer")
     */
    private $flightNum;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="startDatetime", type="datetime")
     */
    private $startDatetime;

    /**
     * @var string
     *
     * @ORM\Column(name="startAirportCode", type="string", length=10)
     */
    private $startAirportCode;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="finishDatetime", type="datetime")
     */
    private $finishDatetime;

    /**
     * @var string
     *
     * @ORM\Column(name="finishAirportCode", type="string", length=10)
     */
    private $finishAirportCode;

    /**
     * @var string
     *
     * @ORM\Column(name="planeCode", type="string", length=10)
     */
    private $planeCode;

    /**
     * @var int
     *
     * @ORM\Column(name="distance", type="integer")
     */
    private $distance;

    /**
     * @var int
     *
     * @ORM\Column(name="durationMin", type="integer")
     */
    private $durationMin;

    /**
     * @var string
     *
     * @ORM\Column(name="price", type="decimal", precision=10, scale=2)
     */
    private $price;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set searchKey
     *
     * @param string $searchKey
     *
     * @return Search
     */
    public function setSearchKey($searchKey)
    {
        $this->searchKey = $searchKey;

        return $this;
    }

    /**
     * Get searchKey
     *
     * @return string
     */
    public function getSearchKey()
    {
        return $this->searchKey;
    }

    /**
     * Set airline
     *
     * @param string $airline
     *
     * @return Search
     */
    public function setAirline($airline)
    {
        $this->airline = $airline;

        return $this;
    }

    /**
     * Get airline
     *
     * @return string
     */
    public function getAirline()
    {
        return $this->airline;
    }

    /**
     * Set flightNum
     *
     * @param integer $flightNum
     *
     * @return Search
     */
    public function setFlightNum($flightNum)
    {
        $this->flightNum = $flightNum;

        return $this;
    }

    /**
     * Get flightNum
     *
     * @return int
     */
    public function getFlightNum()
    {
        return $this->flightNum;
    }

    /**
     * Set startDatetime
     *
     * @param \DateTime $startDatetime
     *
     * @return Search
     */
    public function setStartDatetime($startDatetime)
    {
        $this->startDatetime = $startDatetime;

        return $this;
    }

    /**
     * Get startDatetime
     *
     * @return \DateTime
     */
    public function getStartDatetime()
    {
        return $this->startDatetime;
    }

    /**
     * Set startAirportCode
     *
     * @param string $startAirportCode
     *
     * @return Search
     */
    public function setStartAirportCode($startAirportCode)
    {
        $this->startAirportCode = $startAirportCode;

        return $this;
    }

    /**
     * Get startAirportCode
     *
     * @return string
     */
    public function getStartAirportCode()
    {
        return $this->startAirportCode;
    }

    /**
     * Set finishDatetime
     *
     * @param \DateTime $finishDatetime
     *
     * @return Search
     */
    public function setFinishDatetime($finishDatetime)
    {
        $this->finishDatetime = $finishDatetime;

        return $this;
    }

    /**
     * Get finishDatetime
     *
     * @return \DateTime
     */
    public function getFinishDatetime()
    {
        return $this->finishDatetime;
    }

    /**
     * Set finishAirportCode
     *
     * @param string $finishAirportCode
     *
     * @return Search
     */
    public function setFinishAirportCode($finishAirportCode)
    {
        $this->finishAirportCode = $finishAirportCode;

        return $this;
    }

    /**
     * Get finishAirportCode
     *
     * @return string
     */
    public function getFinishAirportCode()
    {
        return $this->finishAirportCode;
    }

    /**
     * Set planeCode
     *
     * @param string $planeCode
     *
     * @return Search
     */
    public function setPlaneCode($planeCode)
    {
        $this->planeCode = $planeCode;

        return $this;
    }

    /**
     * Get planeCode
     *
     * @return string
     */
    public function getPlaneCode()
    {
        return $this->planeCode;
    }

    /**
     * Set distance
     *
     * @param integer $distance
     *
     * @return Search
     */
    public function setDistance($distance)
    {
        $this->distance = $distance;

        return $this;
    }

    /**
     * Get distance
     *
     * @return int
     */
    public function getDistance()
    {
        return $this->distance;
    }

    /**
     * Set durationMin
     *
     * @param integer $durationMin
     *
     * @return Search
     */
    public function setDurationMin($durationMin)
    {
        $this->durationMin = $durationMin;

        return $this;
    }

    /**
     * Get durationMin
     *
     * @return int
     */
    public function getDurationMin()
    {
        return $this->durationMin;
    }

    /**
     * Set price
     *
     * @param string $price
     *
     * @return Search
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }
}

