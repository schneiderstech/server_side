# README #

Server side App using Symfony.

## Steps to get Symfony up and running ##

To install Symfony Installer we'll run the following commands:

## Linux and macOs systems ##

```$sudo mkdir -p /usr/local/bin```

```$sudo curl -LsS https://symfony.com/installer -o /usr/local/bin/symfony```

```$sudo chmod a+x /usr/local/bin/symfony```

## Windows systems ##

```c:\> php -r "readfile('https://symfony.com/installer');" > symfony```

----------------------------
**For Windows only:**

for example, if WAMP is used

```c:\> move symfony c:\wamp\bin\php```

then, execute the command as:

```c:\> symfony```

moving it to your projects folder

```c:\> move symfony c:\projects```

then, execute the command as

```c:\> cd projects```

```c:\projects\> php symfony```

----------------------------
Once the Symfony Installer is installed, create your first Symfony application with the new command:

```$ symfony new server_side```

Then we go to the project directory and execute this command:

```$ cd server_side/```

```$ php bin/console server:run```